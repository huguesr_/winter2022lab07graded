public class TicTacToeGame{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Welcome to the Tic Tac Toe game!");
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while(!gameOver){
			System.out.println(board);
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			System.out.println("Player "+player+", on which row and column do you want to place your token?");
			int row = reader.nextInt();
			int col = reader.nextInt();
			
			while(!board.placeToken(row, col, playerToken)){
				System.out.println("Please re-enter a row and column");
				row = reader.nextInt();
				col = reader.nextInt();
			}
			
			if(board.checkIfFull()){
				System.out.println("It's a tie!");
				System.out.println(board);
				gameOver = true;
			}
			else if(board.checkIfWinning(playerToken)){
				System.out.println("Player "+player+" is the winner!");
				System.out.println(board);
				gameOver = true;
			}
			else{
				player++;
				if(player >2){
					player = 1;
				}
			}
		}
			
	}
	
}