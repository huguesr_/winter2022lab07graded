public class Board{
	private Square[][] tictactoeBoard = {{Square.BLANK,Square.BLANK,Square.BLANK}, {Square.BLANK,Square.BLANK,Square.BLANK}, {Square.BLANK,Square.BLANK,Square.BLANK} };;
	
	public Board(){
		//tictactoeBoard = new Square[3][3];
		
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if (row>2 || col>2){
			return false;
		}
		else if(tictactoeBoard[row][col] == Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkIfFull(){
		boolean isBlank = true; 
		for(int i=0;i<tictactoeBoard.length; i++){
				for(int j=0;j<tictactoeBoard[i].length; j++){
					if(tictactoeBoard[i][j] == Square.BLANK){
						isBlank = false;
					}
				}
		}
		return isBlank;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		int playerTokenCount = 0;
		for(int i=0;i<tictactoeBoard.length; i++){
				for(int j=0;j<tictactoeBoard[i].length; j++){
					if(tictactoeBoard[i][j] == playerToken){
						playerTokenCount++;
					}
				}
			if(playerTokenCount == 3){
				return true;
			}
			playerTokenCount = 0;
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		int playerTokenCount = 0;
		for(int j=0;j<tictactoeBoard.length; j++){
				for(int i=0;i<tictactoeBoard[j].length; i++){
					if(tictactoeBoard[i][j] == playerToken){
						playerTokenCount++;
					}
				}
			if(playerTokenCount == 3){
				return true;
			}
			playerTokenCount = 0;
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken)){
			return true;
		}
		else if(checkIfWinningVertical(playerToken)){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	
	public String toString(){
		String board = "";
		for(int i=0;i<tictactoeBoard.length; i++){
				for(int j=0;j<tictactoeBoard[i].length; j++){
					board = board+tictactoeBoard[i][j]+" ";
				}
				board = board+"\n";
		}
		return board;
	}
	
	
	
	
	
	
}